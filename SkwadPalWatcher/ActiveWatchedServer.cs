﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using SkwadPalWatcher.Database.Model;
using SkwadPalWatcher.GameDrill.Common;
using SkwadPalWatcher.GameDrill;
using SkwadPalWatcher.GameHandlers;
using DSharpPlus;
using SkwadPalWatcher.Database;
using Microsoft.EntityFrameworkCore;

namespace SkwadPalWatcher;

public class ActiveWatchedServer
{
    Database.Model.WatchedServer WatchedServer;
    public string Name { get => WatchedServer.Name; }
    public ulong ChannelId { get => WatchedServer.ChannelId; }
    public int WatchedServerId { get => WatchedServer.Id; }
    bool keepRunning = true;
    Task monitorTask;
    Drill drill { get; set; }
    ServerInfoResponse? serverResponse;
    DiscordClient discordClient;

    public ActiveWatchedServer(WatchedServer watchedServer, DiscordClient discordClient)
    {
        WatchedServer = watchedServer;
        this.discordClient = discordClient;
        monitorTask = Task.Run(async () =>
        {
            await this.MonitorLoop();
        });
    }

    public async Task ReloadFromDB()
    {
        using(var db = BotDbContext.GetNewContext())
        {
            var wServer = await db.WatchedServers
                .Where(ws => ws.Id == WatchedServer.Id)
                .FirstOrDefaultAsync();
            if (wServer != null)
                WatchedServer = wServer;
        }
    }

    public async Task MonitorLoop()
    {
        var drill = PalWorldHandler.NewDrill();
        var channel = await discordClient.GetChannelAsync(WatchedServer.ChannelId);
        DiscordMessage? message = default;
        if (WatchedServer.ReportingMessageId != null)
        {
            try
            {
                message = await channel.GetMessageAsync((ulong)WatchedServer.ReportingMessageId);
            }
            catch(Exception e)
            {
                Console.WriteLine("DEBUG: Failed refresh.  Perhaps discord isn't ready yet?");
                throw;
            }
        }

        while(keepRunning)
        {
            try
            {
                Console.WriteLine($"DEBUG: Refreshing server: {WatchedServer.EndPoint} => {message?.Id}");
                serverResponse = await drill.QueryServerAsync(WatchedServer.EndPoint);
                var msgBuilder = PalWorldHandler.BuildMessage(WatchedServer, serverResponse);
                if (message == null)
                {
                    message = await channel.SendMessageAsync(msgBuilder);
                    using(var db = BotDbContext.GetNewContext())
                    {
                        WatchedServer.ReportingMessageId = message.Id;
                        db.Update(WatchedServer);
                        await db.SaveChangesAsync();
                    }
                }
                await message.ModifyAsync(msgBuilder);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Thread.Sleep(1000 * 60);
        }
    }

    public void Cancel()
    {
        keepRunning = false;
    }
}
