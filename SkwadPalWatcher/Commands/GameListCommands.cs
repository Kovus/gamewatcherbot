﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;

namespace SkwadPalWatcher.Commands;

public class GameListCommands : ApplicationCommandModule
{
    [SlashCommand("SupportedGameList", "List the game types that are supported currently.")]
    [SlashRequireUserPermissions(Permissions.ManageChannels)]
    public async Task GameList(InteractionContext ctx)
    {
        var response = new DiscordEmbedBuilder();
        response.WithColor(DiscordColor.DarkGreen);
        response.Title = "Available Game Types";
        response.Description = "Currently only supports: PalWorld";
        await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder().AsEphemeral(true).AddEmbed(response));
        return;

    }

}
