﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using Microsoft.EntityFrameworkCore;
using Serilog;
using SkwadPalWatcher.Database;
using SkwadPalWatcher.Database.Model;

namespace SkwadPalWatcher.Commands;

public class ServerWatchCommands : ApplicationCommandModule
{
    [SlashCommand("AddServer", "Register a server entry with the current channel.  Requires ManageChannels privileges.")]
    [SlashRequireUserPermissions(Permissions.ManageChannels)]
    public async Task AddServerWatcher(InteractionContext ctx,
        [Option("name", "Name to provide for this server watcher.  Used when modifying the watcher.")]
        string name,
        [Option("game", "The game type of which to query this server.  (See `/SupportedGameList`)")]
        string game,
        [Option("ipAddress", "IP Address of the server (hostname supported)")]
        string ipAddress,
        [Option("port", "Port to query the server")]
        long port
        )
    {
        await ctx.CreateResponseAsync(
            InteractionResponseType.DeferredChannelMessageWithSource,
            new DiscordInteractionResponseBuilder().AsEphemeral(true));

        WatchedServer server;
        // TODO: Evaluate 'game' to verify it's a game type we support.
        using (var db = BotDbContext.GetNewContext())
        {
            var exist = await db.WatchedServers
                .Where(ws => ws.ChannelId == ctx.Channel.Id && (
                    ws.Name == name ||
                    (ws.Address == ipAddress && ws.Port == port)
                ))
                .FirstOrDefaultAsync();
            if (exist != null)
            {
                var errResponse = new DiscordEmbedBuilder();
                errResponse.WithColor(DiscordColor.IndianRed);
                errResponse.Description = $"Server record already appears to exist.";
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(errResponse));
                return;
            }
            var response = new DiscordEmbedBuilder();
            response.WithColor(DiscordColor.CornflowerBlue);
            response.Description = $"Preparing to query server.";
            var message = await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(response));

            server = new WatchedServer()
            {
                ChannelId = ctx.Channel.Id,
                ReportingMessageId = null,
                Name = name,
                Address = ipAddress,
                Port = (int)port,
                GameType = game,
            };
            try
            {
                db.WatchedServers.Add(server);
                var count = await db.SaveChangesAsync();
                if (count != 1)
                {
                    var errResponse = new DiscordEmbedBuilder();
                    errResponse.WithColor(DiscordColor.IndianRed);
                    errResponse.Description = $"Server record failed to add.";
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(errResponse));
                    return;
                }
            }
            catch(Exception ex)
            {
                Log.Debug(ex.ToString());
                throw;
            }
        }
        Program.serverUpdateManager?.Add(server);
    }

    [SlashCommand("RemoveServer", "Removes a server entry.  Requires ManageChannels privileges.")]
    [SlashRequireUserPermissions(Permissions.ManageChannels)]
    public async Task RemoveServerWatcher(InteractionContext ctx,
        [Option("name", "Name to provide for this server watcher.  Used when modifying the watcher.")]
        string name
        )
    {
        var embed = new DiscordEmbedBuilder();
        using (var db = BotDbContext.GetNewContext())
        {
            var exist = await db.WatchedServers
                .Where(ws => ws.ChannelId == ctx.Channel.Id && ws.Name == name)
                .FirstOrDefaultAsync();
            if (exist != null)
            {
                db.WatchedServers.Remove(exist);
                await db.SaveChangesAsync();
                embed.WithColor(DiscordColor.CornflowerBlue);
                embed.Description = $"Server record removed.";
            }
            else
            {
                embed.WithColor(DiscordColor.IndianRed);
                embed.Description = $"Server record not found.";
            }
        }
        await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
            new DiscordInteractionResponseBuilder()
                .AsEphemeral(true)
                .AddEmbed(embed));
    }

    [SlashCommand("ListServers", "List servers associated with this channel.  Requires ManageChannels privileges.")]
    [SlashRequireUserPermissions(Permissions.ManageChannels)]
    public async Task ListServers(InteractionContext ctx)
    {
        await ctx.CreateResponseAsync(
            InteractionResponseType.DeferredChannelMessageWithSource,
            new DiscordInteractionResponseBuilder().AsEphemeral(true));
        var embed = new DiscordEmbedBuilder();
        using (var db = BotDbContext.GetNewContext())
        {
            var servers = await db.WatchedServers
                .Where(ws => ws.ChannelId == ctx.Channel.Id)
                .ToListAsync();
            if (servers != null && servers.Count > 0)
            {
                embed.WithColor(DiscordColor.CornflowerBlue);
                embed.Description = "Found Servers: \n";
                foreach (var entry in servers)
                {
                    embed.Description += $"Server: {entry.Name}: {entry.Address}:{entry.Port}\n";
                }
            }
            else
            {
                embed.WithColor(DiscordColor.IndianRed);
                embed.Description = $"No servers mapped to this channel.";
            }
        }
        await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
    }

    [SlashCommand("ServerTimezone", "Set the timezone associated with this server.  Requires ManageChannels privileges.")]
    [SlashRequireUserPermissions(Permissions.ManageChannels)]
    public async Task SetTimezone(InteractionContext ctx,
        [Option("name", "Name to provide for this server watcher.  Used when modifying the watcher.")]
        string name,
        [Option("timezone", "Name of the timezone to use with this server.")]
        string timezone
        )
    {
        await ctx.CreateResponseAsync(
            InteractionResponseType.DeferredChannelMessageWithSource,
            new DiscordInteractionResponseBuilder().AsEphemeral(true));

        var embed = new DiscordEmbedBuilder();
        using (var db = BotDbContext.GetNewContext())
        {
            var exist = await db.WatchedServers
                .Where(ws => ws.ChannelId == ctx.Channel.Id && ws.Name == name)
                .FirstOrDefaultAsync();
            if (exist != null)
            {
                exist.SetTimeZoneName(timezone);
                await db.SaveChangesAsync();
                Program.serverUpdateManager?.Reload(exist.Id);
                embed.WithColor(DiscordColor.CornflowerBlue);
                embed.Description = $"Server timezone updated.";
            }
            else
            {
                embed.WithColor(DiscordColor.IndianRed);
                embed.Description = $"Server record not found.";
            }
        }
        await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
    }
}
