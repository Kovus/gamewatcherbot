﻿using DSharpPlus;
using DSharpPlus.SlashCommands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using SkwadPalWatcher.Database;

namespace SkwadPalWatcher;

internal class Program
{
    public static ServerUpdateManager? serverUpdateManager { get; set; }
    private static DiscordClient discord = null;

    static async Task Main(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("DOTNETCORE_ENVIRONMENT") ?? "production"}.json", true)
            .Build();
        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .CreateLogger();

        using (var db = BotDbContext.GetNewContext())
        {
            Log.Information("Migrating Database");
            db.Database.Migrate();
        }

        // Initialize Discord client
        var discord = new DiscordClient(new DiscordConfiguration()
        {
            //Intents = DiscordIntents.DirectMessages,
            Token = configuration.GetValue<string>("Discord:Token"),
            TokenType = TokenType.Bot,
        });
        // Initialize slash command set.
        var slashCommands = discord.UseSlashCommands();
        var debugGuildId = configuration.GetValue<ulong?>("Discord:DebugGuildID");
        if (debugGuildId == null || debugGuildId == 0)
            Log.Warning($"Registering slash commands to a single guild: {debugGuildId}");
        else
            Log.Debug("Registering slash commands for all guilds");
        slashCommands.RegisterCommands<Commands.GameListCommands>(debugGuildId);
        slashCommands.RegisterCommands<Commands.ServerWatchCommands>(debugGuildId);

        await discord.ConnectAsync();

        serverUpdateManager = new ServerUpdateManager(discord, loadFromDb: true);

        await Task.Delay(-1);
    }
}
