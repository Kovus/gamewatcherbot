﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using SkwadPalWatcher.GameDrill;
using Serilog;
using SkwadPalWatcher.Database.Model;

namespace SkwadPalWatcher.GameHandlers;

public class PalWorldHandler
{
    public static Drill NewDrill()
    {
        return new GameDrill.Protocol.PalWorld();
    }

    public static DiscordMessageBuilder BuildMessage(WatchedServer watchedServer, GameDrill.Common.ServerInfoResponse? serverResponse)
    {
        DiscordMessageBuilder buildResponse = new DiscordMessageBuilder();

        if (serverResponse ==  null)
        {
            DiscordEmbedBuilder embed = new DiscordEmbedBuilder();
            embed.WithColor(DiscordColor.IndianRed);
            embed.WithTitle($"No response when querying server.");
            embed.WithTimestamp(DateTimeOffset.Now);
            buildResponse.Embed = embed;
        }
        else if (serverResponse is not GameDrill.Common.EpicOnline.Session)
        {
            DiscordEmbedBuilder embed = new DiscordEmbedBuilder();
            embed.WithColor(DiscordColor.IndianRed);
            embed.WithTitle($"Received an unusuable response.");
            embed.WithTimestamp(DateTimeOffset.Now);
            buildResponse.Embed = embed;
        }
        else
        {
            var session = (GameDrill.Common.EpicOnline.Session) serverResponse;
            var embed = new DiscordEmbedBuilder();
            var adjustedTime = session.Attributes?.UnixCreateTime;
            if (watchedServer.TimeZoneInfo != null)
            {
                // sent as local time, and conver to utc.
                adjustedTime -= (int) watchedServer.TimeZoneInfo.BaseUtcOffset.TotalSeconds;
            }
            embed.WithColor(new DiscordColor("#ffcd9e"));
            embed.Description = $"## {session.Attributes?.Name}\n\n" +
                $"PalWorld game: {session.Attributes?.Address}:{session.Attributes?.GameserverPort}";
            embed.AddField("Players", $"{session.Attributes?.PlayerCount} / {session.Settings?.MaxPublicPlayers}", true);
            embed.AddField("Days", $"{session.Attributes?.Days}", true);
            embed.AddField("Version", $"{session.Attributes?.Version}", true);
            embed.AddField("Last Restart?", $"<t:{adjustedTime}:R>");
            embed.WithTimestamp(DateTimeOffset.UtcNow);
            buildResponse.Embed = embed;
        }

        return buildResponse;
    }
}
