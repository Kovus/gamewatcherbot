﻿using System.Text.Json.Serialization;

namespace SkwadPalWatcher.GameDrill.Common.EpicOnline;

public class DeviceIdResponse
{
    [JsonPropertyName("access_token")]
    public string AccessToken { get; set; }
}