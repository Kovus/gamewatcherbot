﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SkwadPalWatcher.GameDrill.Common.EpicOnline;

public class SessionQueryResponse
{
    [JsonPropertyName("sessions")]
    public List<Session> Sessions { get; set; } = new();
    [JsonPropertyName("count")]
    public int Count { get; set; }
}

public class Session : SkwadPalWatcher.GameDrill.Common.ServerInfoResponse
{
    [JsonPropertyName("deployment")]
    public string DeploymentId { get; set; }
    [JsonPropertyName("id")]
    public string Id { get; set; }
    [JsonPropertyName("bucket")]
    public string Bucket { get; set; }
    [JsonPropertyName("settings")]
    public SessionSettings? Settings { get; set; }
    [JsonPropertyName("totalPlayers")]
    public int TotalPlayers { get; set; }
    [JsonPropertyName("openPublicPlayers")]
    public int OpenPublicPlayers { get; set; }
    [JsonPropertyName("publicPlayers")]
    public List<SessionPlayer>? Players { get; set; }
    [JsonPropertyName("started")]
    public bool Started { get; set; }
    [JsonPropertyName("lastUpdated")]
    public int? LastUpdated { get; set; }
    [JsonPropertyName("attributes")]
    public SessionAttributes? Attributes { get; set; }
    [JsonPropertyName("owner")]
    public string Owner { get; set; } = "";
    [JsonPropertyName("ownerPlatformId")]
    public string? OwnerPlatformId { get; set; }
}

public class SessionSettings
{
    [JsonPropertyName("maxPublicPlayers")]
    public int MaxPublicPlayers { get; set; }
    [JsonPropertyName("allowInvites")]
    public bool AllowInvites { get; set; }
    [JsonPropertyName("shouldAdvertise")]
    public bool ShouldAdvertise { get; set; }
    [JsonPropertyName("allowReadById")]
    public bool AllowReadById { get; set; }
    [JsonPropertyName("allowJoinViaPresence")]
    public bool AllowJoinViaPresence { get; set; }
    [JsonPropertyName("allowJoinInProgress")]
    public bool AllowJoinInProgress { get; set; }
    [JsonPropertyName("allowConferenceRoom")]
    public bool AllowConferenceRoom { get; set; }
    [JsonPropertyName("checkSanctions")]
    public bool CheckSanctions { get; set; }
    [JsonPropertyName("allowMigration")]
    public bool AllowMigration { get; set; }
    [JsonPropertyName("rejoinAfterKick")]
    public string? RejoinAfterKick { get; set; }
    [JsonPropertyName("platforms")]
    public string? Platforms { get; set; }
}

public class SessionPlayer
{

}

public class SessionAttributes
{
    [JsonPropertyName("OWNINGUSERNAME_s")]
    public string OwningUsername { get; set; }
    [JsonPropertyName("GAMESERVER_PORT_l")]
    public int GameserverPort { get; set; }
    [JsonPropertyName("DAYS_l")]
    public int Days { get; set; }
    [JsonPropertyName("NUMPRIVATECONNECTIONS_l")]
    public int NumPrivateConnections { get; set; }
    [JsonPropertyName("TYPE_s")]
    public string Type { get; set; }
    [JsonPropertyName("CREATE_TIME_l")]
    public int UnixCreateTime { get; set; }
    [JsonPropertyName("SERVERTIME_l")]
    public int ServerTime { get; set; }
    [JsonPropertyName("DEDICATEDONLY_b")]
    public bool DedicatedOnly { get; set; }
    [JsonPropertyName("ISPASSWORD_b")]
    public bool HasPassword { get; set; }
    [JsonPropertyName("VERSION_s")]
    public string Version { get; set; }
    [JsonPropertyName("GAMESERVER_ADDRESS_s")]
    public string GameServerAddress { get; set; }
    [JsonPropertyName("NAMESPACE_s")]
    public string Namespace { get; set; }
    [JsonPropertyName("BANTICHEATPROTECTED_b")]
    public bool AntiCheatProtected { get; set; }
    [JsonPropertyName("BUSESSTATS_b")]
    public bool UseStats { get; set; }
    [JsonPropertyName("WORLDGUID_s")]
    public string WorldGUID { get; set; }
    [JsonPropertyName("NAME_s")]
    public string Name { get; set; }
    [JsonPropertyName("ADDRESS_s")]
    public string Address { get; set; }
    [JsonPropertyName("PRESENCESEARCH_b")]
    public bool PresenceSearch { get; set; }
    [JsonPropertyName("NUMPUBLICCONNECTIONS_l")]
    public int NumPublicConnections { get; set; }
    [JsonPropertyName("DESCRIPTION_s")]
    public string Description { get; set; }
    [JsonPropertyName("BUILDUNIQUEID_l")]
    public int BuildUniqueId { get; set; }
    [JsonPropertyName("BISDEDICATED_b")]
    public bool IsDedicated { get; set; }
    [JsonPropertyName("PLAYERS_l")]
    public int PlayerCount { get; set; }
    [JsonPropertyName("MAPNAME_s")]
    public string MapName { get; set; }

}
