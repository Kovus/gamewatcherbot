﻿using System.Text.Json.Serialization;

namespace SkwadPalWatcher.GameDrill.Common.EpicOnline;

public class AuthenticationResponse
{
    [JsonPropertyName("access_token")]
    public string AccessToken { get; set; }
    [JsonPropertyName("token_type")]
    public string TokenType { get; set; }
    [JsonPropertyName("expires_at")]
    public DateTime ExpiresAt { get; set; }
    [JsonPropertyName("features")]
    List<string>? Features { get; set; } = new();
    [JsonPropertyName("organization_id")]
    public string OrganizationId { get; set; }
    [JsonPropertyName("product_id")]
    public string ProductId { get; set; }
    [JsonPropertyName("sandbox_id")]
    public string SandboxId { get; set; }
    [JsonPropertyName("deployment_id")]
    public string DeploymentId { get; set; }
    // These are specific to device logins:
    [JsonPropertyName("nonce")]
    public string Nonce { get; set; }
    [JsonPropertyName("organization_user_id")]
    public string OrganizationUserId { get; set; }
    [JsonPropertyName("product_user_id")]
    public string ProductUserId { get; set; }
    [JsonPropertyName("product_user_id_created")]
    public bool ProductUserIdCreated { get; set; }
    [JsonPropertyName("id_token")]
    public string IdToken { get; set; }
}