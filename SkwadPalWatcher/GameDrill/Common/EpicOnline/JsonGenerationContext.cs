﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace SkwadPalWatcher.GameDrill.Common.EpicOnline;

[JsonSourceGenerationOptions(WriteIndented = true)]
[JsonSerializable(typeof(AuthenticationResponse))]
[JsonSerializable(typeof(DeviceIdResponse))]
[JsonSerializable(typeof(QueryFilter))]
[JsonSerializable(typeof(FilterCriteriaBase))]
[JsonSerializable(typeof(StringFilterCriteria))]
[JsonSerializable(typeof(LongFilterCriteria))]
[JsonSerializable(typeof(SessionQueryResponse))]
[JsonSerializable(typeof(Session))]
[JsonSerializable(typeof(SessionSettings))]
[JsonSerializable(typeof(SessionPlayer))]
[JsonSerializable(typeof(SessionAttributes))]
public partial class JsonGenerationContext : JsonSerializerContext
{

}
