﻿using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text.Json;
using SkwadPalWatcher.GameDrill;

namespace SkwadPalWatcher.GameDrill.Common.EpicOnline;

public abstract class EpicOnline : Drill
{
    public static string RootUrl { get; set; } = "https://api.epicgames.dev";

    abstract public string clientId { get; set; }
    abstract public string clientSecret { get; set; }
    abstract public string deploymentId { get; set; }

    private string nonce = RandomString(22);

    // Some games require a device token rather than user token.
    abstract public bool useDeviceToken { get; init; }
    protected AuthenticationResponse? authResponse = default;

    public EpicOnline ()
    {
        useDeviceToken = false;
    }

    private static string RandomString(int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return RandomNumberGenerator.GetString(chars, length);
    }

    public async Task FetchClientTokenAsync()
    {
        if (useDeviceToken)
        {
            throw new InvalidOperationException("This game type uses device tokens, not client tokens.");
        }

        var authReq = new Request("POST", RootUrl, "/auth/v1/oauth/token", PayloadDict: new()
        {
            { "grant_type", "client_credentials" },
            { "deployment_id", deploymentId },
        }, ContentType: "application/x-www-form-urlencoded");
        authReq.AuthToken = new BasicAuthenticationHeaderValue(clientId, clientSecret);
        var authResult = await authReq.GetResponseAsync<AuthenticationResponse>();
        if (authResult != null)
        {
            authResponse = authResult;
        }
        else
        {
            throw new IOException("Failed to get authentication response");
        }
    }

    public async Task FetchDeviceTokenAsync()
    {
        if (useDeviceToken == false)
        {
            throw new InvalidOperationException("This game type uses client tokens, not device tokens.");
        }

        var deviceReq = new Request("POST", RootUrl, "/auth/v1/accounts/deviceid", PayloadStr: "deviceModel=PC", ContentType: "application/x-www-form-urlencoded");
        deviceReq.AuthToken = new BasicAuthenticationHeaderValue(clientId, clientSecret);
        var deviceResult = await deviceReq.GetResponseAsync<DeviceIdResponse>();
        if (deviceResult == null)
        {
            throw new IOException("Failed to get a DeviceId response");
        }

        var authReq = new Request("POST", RootUrl, "/auth/v1/oauth/token", PayloadDict: new()
        {
            { "grant_type", "external_auth" },
            { "external_auth_type", "deviceid_access_token" },
            { "external_auth_token", deviceResult.AccessToken },
            { "nonce", "ABCHFA3qgUCJ1XTPAoGDEF" }, //nonce }, // required, but unique to us.
            { "deployment_id", deploymentId },
            { "display_name", "User" },
        }, ContentType: "application/x-www-form-urlencoded");
        authReq.AuthToken = new BasicAuthenticationHeaderValue(clientId, clientSecret);
        var authResult = await authReq.GetResponseAsync<AuthenticationResponse>();
        if (authResult != null)
        {
            authResponse = authResult;
        }
        else
        {
            throw new IOException("Failed to get authentication response");
        }
    }

    public override async Task<ServerInfoResponse?> QueryServerAsync(IPEndPoint serverEndPoint, bool authAsNecessary=true)
    {
        if(authResponse == null && authAsNecessary == false)
        {
            throw new InvalidDataException("No authentication data available.");
        }
        if(authResponse == null || authResponse.ExpiresAt < DateTime.UtcNow)
        {
            if (useDeviceToken)
                await FetchDeviceTokenAsync();
            else
                await FetchClientTokenAsync();
        }
        if(authResponse == null)
        {
            throw new InvalidDataException("Failed to get authentication data.");
        }

        var filter = new QueryFilter()
        {
            Criteria = new() {
                new StringFilterCriteria()
                {
                    Key = "attributes.ADDRESS_s",
                    Operation = "EQUAL",
                    Value = serverEndPoint.Address.ToString()
                },
                new LongFilterCriteria()
                {
                    Key = "attributes.GAMESERVER_PORT_l",
                    Operation = "EQUAL",
                    Value = serverEndPoint.Port,
                }
            }
        };
        var filterStr = JsonSerializer.Serialize(filter, typeof(QueryFilter), JsonGenerationContext.Default);
        var queryReq = new Request("POST", RootUrl, $"/matchmaking/v1/{deploymentId}/filter", PayloadStr: filterStr, ContentType: "application/json");
        queryReq.AuthToken = new AuthenticationHeaderValue("Bearer", authResponse.AccessToken);

        var queryResult = await queryReq.GetResponseAsync<SessionQueryResponse>();
        if (queryResult != null)
        {
            return queryResult.Sessions.FirstOrDefault();
        }
        throw new IOException("Failed to get a valid session response");
    }
}
