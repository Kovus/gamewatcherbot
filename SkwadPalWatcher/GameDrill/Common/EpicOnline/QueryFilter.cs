﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace SkwadPalWatcher.GameDrill.Common.EpicOnline;

public class QueryFilter
{
    [JsonPropertyName("criteria")]
    public List<FilterCriteriaBase> Criteria { get; set; }
}

[JsonDerivedType(typeof(StringFilterCriteria))]
[JsonDerivedType(typeof(LongFilterCriteria))]
public class FilterCriteriaBase
{
    [JsonPropertyOrder(1)]
    [JsonPropertyName("key")]
    public string Key { get; set; }
    [JsonPropertyOrder(2)]
    [JsonPropertyName("op")]
    public string Operation { get; set; }
}

public class StringFilterCriteria : FilterCriteriaBase
{
    [JsonPropertyOrder(3)]
    [JsonPropertyName("value")]
    public string Value { get; set; }
}

public class LongFilterCriteria : FilterCriteriaBase
{
    [JsonPropertyName("value")]
    public int Value { get; set; }
}

public class BooleanFilterCriteria : FilterCriteriaBase
{
    [JsonPropertyName("value")]
    public bool Value { get; set; }
}


