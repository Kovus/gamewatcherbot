﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using System.Collections.Generic;
using System.Text.Json;
using System.Net.Http.Headers;
using SkwadPalWatcher.GameDrill.Common.EpicOnline;
using SkwadPalWatcher.GameDrill.Extensions;

namespace SkwadPalWatcher.GameDrill.Common;

public class Request
{
    public static readonly HttpClient httpClient;
    public static bool debug = false;

    static Request()
    {
        HttpClientHandler handler = new HttpClientHandler()
        {
            AutomaticDecompression = DecompressionMethods.All,
        };
        httpClient = new HttpClient(handler);
        httpClient.Timeout = TimeSpan.FromSeconds(300.0);
    }

    public HttpStatusCode StatusCode
    {
        get;
        private set;
    } = HttpStatusCode.UnavailableForLegalReasons;

    public string ResponsePayload
    {
        get;
        private set;
    }

    private HttpMethod Method = HttpMethod.Get;
    private string RootUrl;
    private string EndPoint;
    private Dictionary<string, string> PayloadDictionary;
    private string? PayloadString = null;
    private string ContentType;
    public AuthenticationHeaderValue? AuthToken { get; set; }

    public Request(string Method, string RootUrl, string EndPoint, Dictionary<string, string>? PayloadDict = null, string? PayloadStr = null, string ContentType = "text/plain")
    : this(MethodByString(Method), RootUrl, EndPoint, PayloadDict, PayloadStr, ContentType)
    { }

    public Request(HttpMethod Method, string RootUrl, string EndPoint, Dictionary<string, string>? PayloadDict = null, string? PayloadStr = null, string ContentType = "text/plain")
    {
        this.Method = Method;
        this.RootUrl = RootUrl;
        this.EndPoint = EndPoint;
        if (PayloadDict != null)
            PayloadDictionary = PayloadDict;
        PayloadString = PayloadStr;
        this.ContentType = ContentType;
    }

    public static HttpMethod MethodByString(string method)
    {
        if (method.ToUpper() == "GET")
            return HttpMethod.Get;
        if (method.ToUpper() == "POST")
            return HttpMethod.Post;
        if (method.ToUpper() == "PUT")
            return HttpMethod.Put;
        if (method.ToUpper() == "DELETE")
            return HttpMethod.Delete;
        if (method.ToUpper() == "HEAD")
            return HttpMethod.Head;
        if (method.ToUpper() == "OPTIONS")
            return HttpMethod.Options;
        if (method.ToUpper() == "PATCH")
            return HttpMethod.Patch;
        throw new InvalidDataException($"Unknown HTTP method '{method}'.");
    }

    public async Task<JsonRepr?> GetResponseAsync<JsonRepr>()
    {
        return await GetResponseAsync<JsonRepr>(CancellationToken.None);
    }

    // TODO: Fix this; payload changed from string to dictionary.

    public async Task<JsonRepr?> GetResponseAsync<JsonRepr>(CancellationToken ct)
    {
        try
        {
            string result = await GetResultAsStringAsync(ct);
            return (JsonRepr?)JsonSerializer.Deserialize(result, typeof(JsonRepr), JsonGenerationContext.Default);
        }
        catch (JsonException ex)
        {
            Console.Write($"Error deserializing json: {ex.Message}\n");
        }
        catch
        {
            // @TODO: Log?
            Console.Write($"Unknown error deserializing json.\n");
        }

        return default;
    }

    public string GetResultAsString()
    {
        return GetResultAsStringAsync().Result;
    }

    public async Task<string> GetResultAsStringAsync()
    {
        return await GetResultAsStringAsync(CancellationToken.None);
    }

    public async Task<string> GetResultAsStringAsync(CancellationToken ct)
    {
        StatusCode = HttpStatusCode.UnavailableForLegalReasons;
        ResponsePayload = "";

        try
        {
            Uri path = new Uri(RootUrl + EndPoint);
            if (Method == HttpMethod.Get && PayloadDictionary != null)
            {
                // Payload is treated as a query string for Get requests.
                foreach (var entry in PayloadDictionary)
                {
                    path = path.AddQuery(entry.Key, entry.Value);
                }
            }
            if (debug)
                Console.WriteLine("DEBUG: Preparing query: " + path.ToString());

            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, path))
            {
                httpRequestMessage.Headers.Add("accept", "*/*");
                httpRequestMessage.Headers.Add("user-agent", "GameOwls Server Querier");
                if (AuthToken != null)
                {
                    httpRequestMessage.Headers.Authorization = AuthToken;
                }
                if (Method != HttpMethod.Get && PayloadDictionary != null)
                {
                    if (ContentType == "application/x-www-form-urlencoded")
                        httpRequestMessage.Content = new FormUrlEncodedContent(PayloadDictionary);
                    else
                        httpRequestMessage.Content = new StringContent(PayloadDictionary.ToString(), Encoding.UTF8, ContentType);
                    httpRequestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(ContentType);
                }
                if (Method != HttpMethod.Get && !string.IsNullOrEmpty(PayloadString))
                {
                    httpRequestMessage.Content = new StringContent(PayloadString, Encoding.UTF8, ContentType);
                    httpRequestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(ContentType);
                }

                using (Task<HttpResponseMessage> queryTask = httpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, ct))
                {
                    HttpResponseMessage response = queryTask.Result;
                    StatusCode = response.StatusCode;
                    ResponsePayload = await response.Content.ReadAsStringAsync(ct);
                }
            }
        }
        catch (HttpRequestException e)
        {
            if (e.StatusCode != null)
            {
                StatusCode = (HttpStatusCode)e.StatusCode;
            }
        }
        catch (TaskCanceledException)
        {
            StatusCode = HttpStatusCode.RequestTimeout;
            // @TODO: Log?
        }

        return ResponsePayload;
    }

    public async Task GetResultNoResponse()
    {
        await GetResultNoResponse(CancellationToken.None);
    }

    public async Task GetResultNoResponse(CancellationToken ct)
    {
        StatusCode = HttpStatusCode.UnavailableForLegalReasons;
        ResponsePayload = null;

        try
        {
            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, RootUrl + EndPoint))
            {
                if (AuthToken != null)
                {
                    httpRequestMessage.Headers.Authorization = AuthToken;
                }
                if (!string.IsNullOrWhiteSpace(PayloadDictionary.ToString()))
                {
                    httpRequestMessage.Content = new StringContent(PayloadDictionary.ToString(), Encoding.UTF8, ContentType);
                }

                using (HttpResponseMessage response = await httpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, ct))
                {
                    StatusCode = response.StatusCode;
                }
            }
        }
        catch (HttpRequestException exc)
        {
            if (exc.StatusCode != null)
            {
                StatusCode = (HttpStatusCode)exc.StatusCode;
                if (debug)
                    Console.WriteLine("DEBUG: HttpRequestException: " + exc.Message);
            }
        }
        catch (TaskCanceledException exc)
        {
            StatusCode = HttpStatusCode.RequestTimeout;
            if (debug)
                Console.WriteLine("WARNING: GetResultNoResponse task cancelled: " + exc.Message);
        }
    }
}
