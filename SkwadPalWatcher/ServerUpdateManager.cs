﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using SkwadPalWatcher.Database;
using SkwadPalWatcher.Database.Model;

namespace SkwadPalWatcher;

public class ServerUpdateManager
{
    public List<ActiveWatchedServer> serverWatches = new();
    List<Task> watcherThreads = new();
    private DiscordClient discordClient;

    public ServerUpdateManager(DiscordClient dClient, bool loadFromDb=true)
    {
        discordClient = dClient;

        using(var db = BotDbContext.GetNewContext())
        {
            var servers = db.WatchedServers
                .Where(ws => ws.ChannelId != null && ws.ReportingMessageId != null)
                .ToList();
            foreach (var server in servers)
            {
                Console.WriteLine($"DEBUG: Adding server watcher: {server.EndPoint}");
                var watch = new ActiveWatchedServer(server, discordClient);
                serverWatches.Add(watch);
            }
        }
    }

    public void Add(WatchedServer server)
    {
        var watch = new ActiveWatchedServer(server, discordClient);
        serverWatches.Add(watch);
    }

    public void Reload(string serverName, ulong channelId)
    {
        var server = serverWatches.Find(aws => aws.Name == serverName && aws.ChannelId == channelId);
        if (server != null)
        {
            server.ReloadFromDB();
        }
    }
    public void Reload(int serverId)
    {
        var server = serverWatches.Find(aws => aws.WatchedServerId == serverId);
        if (server != null)
        {
            server.ReloadFromDB();
        }
    }
}
