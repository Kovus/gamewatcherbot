﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SkwadPalWatcher.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class InitialServerWatcherTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "watched_servers",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    channel_id = table.Column<ulong>(type: "INTEGER", nullable: false),
                    name = table.Column<string>(type: "TEXT", nullable: false),
                    address = table.Column<string>(type: "TEXT", nullable: false),
                    port = table.Column<int>(type: "INTEGER", nullable: false),
                    game_type = table.Column<string>(type: "TEXT", nullable: false),
                    reporting_message_id = table.Column<ulong>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_watched_servers", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_watched_servers_channel_id_address_port",
                table: "watched_servers",
                columns: new[] { "channel_id", "address", "port" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_watched_servers_channel_id_name",
                table: "watched_servers",
                columns: new[] { "channel_id", "name" },
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "watched_servers");
        }
    }
}
