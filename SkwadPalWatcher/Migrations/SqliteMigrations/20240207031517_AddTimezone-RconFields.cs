﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SkwadPalWatcher.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class AddTimezoneRconFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "new_message_seconds",
                table: "watched_servers",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "rcon_password",
                table: "watched_servers",
                type: "TEXT",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "rcon_port",
                table: "watched_servers",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "time_zone_name",
                table: "watched_servers",
                type: "TEXT",
                maxLength: 255,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "new_message_seconds",
                table: "watched_servers");

            migrationBuilder.DropColumn(
                name: "rcon_password",
                table: "watched_servers");

            migrationBuilder.DropColumn(
                name: "rcon_port",
                table: "watched_servers");

            migrationBuilder.DropColumn(
                name: "time_zone_name",
                table: "watched_servers");
        }
    }
}
