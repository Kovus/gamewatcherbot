﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SkwadPalWatcher.Database.Model;

[Index(nameof(ChannelId), nameof(Name), IsUnique = true)]
[Index(nameof(ChannelId), nameof(Address), nameof(Port), IsUnique = true)]
public class WatchedServer
{
    [Key]
    public int Id { get; set; }

    public ulong ChannelId { get; set; }
    [MaxLength(255)]
    public string Name { get; set; }
    [MaxLength(255)]
    public string Address { get; set; }
    public int Port { get; set; }
    [MaxLength(30)]
    public string GameType { get; set; }

    public ulong? ReportingMessageId { get; set; }
    public int NewMessageSeconds { get; set; } = 0;

    [MaxLength(255)]
    public string? TimeZoneName { get; private set; }
    [NotMapped]
    private TimeZoneInfo? _timeZoneInfo { get; set; }
    public void SetTimeZoneName(string? name)
    {
        TimeZoneName = name;
        _timeZoneInfo = null;
    }
    [NotMapped]
    public TimeZoneInfo? TimeZoneInfo
    {
        get
        {
            if (_timeZoneInfo == null && TimeZoneName != null)
            {
                try
                {
                    _timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneName);
                }
                catch
                {
                    _timeZoneInfo = null;
                }
            }
            return _timeZoneInfo;
        }
    }

    public int? RconPort { get; set; }
    [MaxLength(100)]
    public string? RconPassword { get; set; }

    [NotMapped]
    public IPEndPoint EndPoint { get => new IPEndPoint(IPAddress.Parse(Address), Port); }
}
