﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SkwadPalWatcher.Database.Model;

namespace SkwadPalWatcher.Database;

public class BotDbContext : DbContext
{
    public DbSet<WatchedServer> WatchedServers { get; set; }

    public BotDbContext()
    { }

    public BotDbContext(DbContextOptions<BotDbContext> options)
    : base(options)
    { }

    public static BotDbContext GetNewContext()
    {
        //switch (Globals.DatabaseType)
        //{
        //    case DatabaseType.Postgres:
        //        return new PostgresDataContext();
        //    case DatabaseType.Sqlite:
                return new SqliteDataContext();
        //    default:
        //        throw new InvalidOperationException("No database type defined.");
        //}
    }

    public static DbContextOptionsBuilder ApplyOptions(DbContextOptionsBuilder options)
    {
        //switch (Globals.DatabaseType)
        //{
        //    case DatabaseType.Postgres:
        //        return options.UseNpgsql(Globals.DatabaseConnectionString, o => o.CommandTimeout(Globals.CommandTimeout));
        //    case DatabaseType.Sqlite:
                return options.UseSqlite($"Data Source=bot.db", o => o.CommandTimeout(300));
        //    default:
        //        throw new InvalidOperationException("No database type defined.");
        //}
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        options
            .UseSqlite($"Data Source=bot.db")
            .UseSnakeCaseNamingConvention()
            .UseLazyLoadingProxies();
    }
}
