﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SkwadPalWatcher.Database;

public class SqliteDataContext : BotDbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        options
            .UseSqlite($"Data Source=bot.db")
            .UseSnakeCaseNamingConvention()
            .UseLazyLoadingProxies();
    }
}
